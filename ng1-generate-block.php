<?php
/*
 * Plugin Name: Ng1 Generate Block
 * Plugin URI: http://www.ng1.fr
 * Description: Créer des blocks basics dans le template
 * Version: The Plugin's Version Number, e.g.: 1.0
 * Author: nicolas GEHIN
 * Author URI: http://www.ng1.fr
 * License: A "Slug" license name e.g. GPL2
 */

add_action('plugins_loaded', 'ng1_generate_block_init');

function ng1_generate_block_init() {
	if (! class_exists( 'NG1_Toolbox' ) || ! class_exists( 'ACF' )) {
		add_action( 'admin_notices', 'ng1_acf_block__error' );
		return;
	}else{
		Global $ng1_generate_blocks;
		$ng1_generate_blocks = new  Ng1_Generate_Block();
	}

}

class Ng1_Generate_Block {
	public function __construct() {
		$this->add_option_page_ng1();
		NG1_Toolbox::registerFields(__DIR__.'/acf-fields.json');
		add_action('acf/save_post',  array($this, "generate_block"));
	}
	/* ------------------------------------------
	   AJout de la page d'option
	------------------------------------------ */
	public function add_option_page_ng1(){
	 	if( function_exists('acf_add_options_page') ) {
			acf_add_options_sub_page( array(
				'page_title' 	=> "Create blocks",
				'menu_title'	=> 'Create blocks',
				'menu_slug' 	=> 'ng1-create-block',
				'capability'	=> 'edit_posts',
				'icon_url' => 'dashicons-block-default',
				'redirect'		=> false,
				'parent_slug' => 'ng1',
				'post_id' => "generate_block"
			));
		}
	 }
	 public function generate_block($post_id)
	 {

		if ($post_id=="generate_block") {
			$theme_folder = get_stylesheet_directory();

			extract(get_fields("generate_block"));
			$block_identifier =strtolower(str_replace(" ","-",$block_name));
			$block_folder_path = $theme_folder."/acf-blocks/".str_replace(" ","-",$block_identifier);

			if (!file_exists("/acf-blocks/".str_replace(" ","-",$block_identifier))) {
			    mkdir($block_folder_path, 0777, true);
			    $index= fopen($block_folder_path."/index.php",'c+b');
			    $index_content = file_get_contents(__DIR__."/sample/index.php");
				$index_content =str_replace("%name%",$block_identifier,$index_content);
				extract($params);
				foreach ($params as $key => $value) {
					if (is_string($value)) {
						$index_content =str_replace("%".$key."%", $value,$index_content);
					}
				}
				fwrite($index, $index_content);

			if ($use_css=="true" || $use_js=="true") {
				mkdir($block_folder_path."/assets", 0777, true);
				if ($use_css=="true") {
					mkdir($block_folder_path."/assets/css", 0777, true);
					mkdir($block_folder_path."/assets/scss", 0777, true);
					fopen($block_folder_path."/assets/css/style.css",w);
					$file_scss = fopen($block_folder_path."/assets/scss/style.scss",w);
					fwrite($file_scss,file_get_contents(__DIR__."/sample/style.scss"));
					$file_mixins = fopen($block_folder_path."/assets/scss/_mixins.scss",w);
					fwrite($file_mixins, file_get_contents(__DIR__."/sample/_mixins.scss"));
				}
				if ($use_js=="true") {
					mkdir($block_folder_path."/assets/js", 0777, true);
					$file_js = fopen($block_folder_path."/assets/js/function.js",w);
					fwrite($file_js, file_get_contents(__DIR__."/sample/function.js"));
				}
			}
			}

		}

	 	return $post_id;
	 }



}